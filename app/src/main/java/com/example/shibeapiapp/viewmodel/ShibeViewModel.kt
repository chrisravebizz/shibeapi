package com.example.shibeapiapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibeapiapp.model.ShibeRepo
import com.example.shibeapiapp.model.response.DogsDTO
import com.example.shibeapiapp.view.shibe.ShibeState
import kotlinx.coroutines.launch

class ShibeViewModel : ViewModel() {

    // Get instance of repo
    private val repo by lazy { ShibeRepo }
    private val _state = MutableLiveData(ShibeState(isLoading = true))
    val state: LiveData<ShibeState> get() = _state

    init {
        viewModelScope.launch {
            val dogDTO = repo.getDogImages()
            _state.value = ShibeState(dogs = dogDTO)
        }
    }
}