package com.example.shibeapiapp.model

import com.example.shibeapiapp.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ShibeRepo {

    // Get instance of our service
    private val shibeService by lazy { ShibeService.getInstance() }

    suspend fun getDogImages() = withContext(Dispatchers.IO) {
        shibeService.getDogImages()
    }

}