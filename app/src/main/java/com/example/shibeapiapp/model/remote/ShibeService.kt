package com.example.shibeapiapp.model.remote

import com.example.shibeapiapp.model.response.DogsDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val QUERY_COUNT = "count"

        fun getInstance() : ShibeService {
            val retrofit : Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }
    }

    @GET("/api/shibes")
    suspend fun getDogImages(@Query(QUERY_COUNT) dog : Int = 100) : DogsDTO

}