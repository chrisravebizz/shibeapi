package com.example.shibeapiapp.view.shibe

import com.example.shibeapiapp.model.response.DogsDTO

class ShibeState(
    val isLoading : Boolean = false,
    val dogs : DogsDTO = DogsDTO()
)