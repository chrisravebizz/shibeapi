package com.example.shibeapiapp.view.shibe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.shibeapiapp.adapter.ShibeAdapter
import com.example.shibeapiapp.databinding.FragmentShibeBinding
import com.example.shibeapiapp.viewmodel.ShibeViewModel

class ShibeFragment : Fragment() {

    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by viewModels<ShibeViewModel>()
    private val shibeAdapter by lazy { ShibeAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.loader.isVisible = state.isLoading
            binding.rvCard.adapter = shibeAdapter.apply { loadCategory(state.dogs) }
        }
    }

}