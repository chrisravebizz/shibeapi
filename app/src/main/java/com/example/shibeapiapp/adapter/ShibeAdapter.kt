package com.example.shibeapiapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shibeapiapp.databinding.ItemCardBinding
import com.example.shibeapiapp.model.response.DogsDTO
import com.squareup.picasso.Picasso

class ShibeAdapter : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var dog = mutableListOf<String>()

    /* Set up View Holder */
    // This method inflates card layout items for Recycler View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ShibeViewHolder.getInstance(parent)

    // This method sets the data to specific views of card items.
    // It also handles methods related to clicks on items of Recycler view.
    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val dogData = dog[position]
        holder.bindCategory(dogData)
    }

    // This method returns the length of the RecyclerView.
    override fun getItemCount(): Int {
        return dog.size
    }

    // === Custom Function to pass object[it] ===
    // viewModel gets dog and assigns data to dog variable
    fun loadCategory(item: List<String>) {
        this.dog = item.toMutableList()
        notifyDataSetChanged()
    }

    class ShibeViewHolder(
        private val binding: ItemCardBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        // This function gets the data from dog member variable / position and
        // passes data as an argument to this function
        fun bindCategory(dog: String) {
            Picasso.get().load(dog).into(binding.dogThumb)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCardBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ShibeViewHolder(it) }
        }
    }
}